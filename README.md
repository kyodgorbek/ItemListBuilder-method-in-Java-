# ItemListBuilder-method-in-Java-



import java.util.ArrayList;
import org.w3c.dom.Document;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;

public class ItemListBuilderTest {
  
  public static void main(String[] args) throws Exception{
   ArrayList items = new ArrayList();
   items.add(new Item(new Product("Toaster", 29.95), 3));
   items.add(new Item(new Product("Hair dryer", 24.95), 1));
   
   ItemListBuilder builder = new ItemListBuilder();
   Document doc = builder.build(items);
   Transfer t = TransformerFactory .newInstance().newTransformer();
   t.transform(new DOMSource(doc),
   new StreamResult(System.out));
   }
 }   
   
   
  
